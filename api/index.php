<?php
require 'Slim/Slim.php';
require 'RedBean/rb.php';
require '../libs/external/einutech.Extensions.php';
require '../config/config.php';

\Slim\Slim::registerAutoloader();

R::setup('mysql:host='.DB_HOST.'; dbname='.DB_NAME, DB_USER, DB_PASS);
R::freeze(true);

function formatString($input) {
    $output = $input;

    $pentium = "Pentium";
    if (substr($input, 0, strlen($pentium)) === $pentium) {
        $output = preg_replace("/(dual-core)/i", "", $input);
    }

    $output = preg_replace("/(intel|nvidia|ati|amd)/i", "", $output);
    $output = preg_replace("/(™|®|©|&trade;|&reg;|&copy;|&#8482;|&#174;|&#169;|\(TM\)|\(R\))/i", "", $output);
    $output = preg_replace("/(CPU|Series|Radeon|Edition|GeForce|Graphics|\@|&#64;)/i", "", $output);
    $output = preg_replace("/(\d*.\d*GHz)/i", "", $output);
    $output = preg_replace("/(\d*MB)/i", "", $output);
    $output = preg_replace("/(\d*GB)/i", "", $output);
    $output = preg_replace('/^((?=^)(\s*))|((\s*)(?>$))/s', '', $output);
    $output = preg_replace('/\s+/', ' ', $output);

    return $output;
}

function getGPURank($gpu) {
    $output = formatString($gpu);

    $gpuQuery = R::findOne(DB_TABLE_PREFIX.'gpu', 'Model LIKE :model', array(':model' => '%'.$output.'%'));

    if (!empty($gpuQuery)) {
        return $gpuQuery->rank;
    } else {
        return 0;
    }
}

function getCPURank($cpu) {
    $output = formatString($cpu);

    $cpuQuery = R::findOne(DB_TABLE_PREFIX.'cpu', 'Model LIKE :model', array(':model' => '%'.$output.'%'));

    if (!empty($cpuQuery)) {
        return $cpuQuery->rank;
    } else {
        return 0;
    }
}

function getInfoByRank($type, $rank) {
    $query = R::getRow('SELECT * FROM '.DB_TABLE_PREFIX.$type.' WHERE rank = '.$rank);

    return $query;
}

function getInfoByID($type, $id) {
    $query = R::getRow('SELECT * FROM '.DB_TABLE_PREFIX.$type.' WHERE id = '.$id);

    return $query;
}

function getAllUnderID($type, $id) {
    $query = R::getAll('SELECT * FROM '.DB_TABLE_PREFIX.$type.' WHERE id <= '.$id.' ORDER BY id DESC');

    return $query;
}

function getOSRank($os) {
    $pieces = explode(" ", $os);
    $first_part = implode(" ", array_splice($pieces, 0, 3));

    $osQuery = R::findOne(DB_TABLE_PREFIX.'os', 'version LIKE :version', array(':version' => '%'.$first_part.'%'));

    if (!empty($osQuery)) {
        return $osQuery->rank;
    } else {
        return 'OS NOT FOUND';
    }
}

function getGPUID($gpu) {
    $output = formatString($gpu);

    $gpuQuery = R::findOne(DB_TABLE_PREFIX.'gpu', 'Model LIKE :model', array(':model' => '%'.$output.'%'));

    if (!empty($gpuQuery)) {
        return $gpuQuery->id;
    } else {
        return 0;
    }
}

function getCPUID($cpu) {
    $output = formatString($cpu);

    $cpuQuery = R::findOne(DB_TABLE_PREFIX.'cpu', 'Model LIKE :model', array(':model' => '%'.$output.'%'));

    if (!empty($cpuQuery)) {
        return $cpuQuery->id;
    } else {
        return 0;
    }
}

$app = new \Slim\Slim();

$app->get('/test/getInfoByRank/:rank', function ($rank) use ($app) {
    $info = getInfoByRank('gpu', $rank);
    echo $info['Model'];
});

$app->get('/test/formatString/:model', function ($model) use ($app) {
    echo formatString($model);
});

$app->post('/admin/insertRequirements', function() use ($app) {
    $newReq = R::dispense(DB_TABLE_PREFIX.'requirements');

    $newReq->type = $app->request->post('Type');;
    $newReq->software_id = $app->request->post('SoftwareID');
    $newReq->cpu_intel = $app->request->post('CPU_Intel');
    $newReq->cpu_amd = $app->request->post('CPU_AMD');
    $newReq->gpu_nvidia = $app->request->post('GPU_Nvidia');
    $newReq->gpu_amd = $app->request->post('GPU_AMD');
    $newReq->ram = $app->request->post('RAM');
    $newReq->free_hdd = $app->request->post('HDD');
    $newReq->os = $app->request->post('OS');

    R::store($newReq);

    header('Location: http://benchtest.singularminds.eu/admin/requirements');
});

$app->post('/admin/insertSoftware', function() use ($app) {
    $newSoftware = R::dispense(DB_TABLE_PREFIX.'software');

    $newSoftware->type = $app->request->post('Type');
    $newSoftware->name = $app->request->post('Name');
    $newSoftware->description = $app->request->post('Desc');
    $newSoftware->image = $app->request->post('Image');

    R::store($newSoftware);

    echo 'ID OF THIS SOFTWARE IS: <strong>'.$newSoftware->id.'</strong>';
    echo '<br><a href="http://benchtest.singularminds.eu/admin/requirements/'.$newSoftware->id.'">NEXT STEP</a>';
});



$app->post('/compare', function() use ($app) {
    $component1 = $app->request->post('component1');
    $component2 = $app->request->post('component2');
    $type       = $app->request->post('type');

    $app->response()->header('Content-Type', 'application/json');
    $response = array();

    switch($type) {
        case 'cpu':
            if ((int)getCPURank($component1) == 0 || (int)getCPURank($component2) == 0) {
                $response['result'] = 'false';
            } else {
                if ((int)getCPURank($component1) <= (int)getCPURank($component2)) {
                    $response['result'] = 'true';
                } else {
                    $response['result'] = 'false';
                }
            }
            break;

        case 'gpu':
        if ((int)getGPURank($component1) == 0 || (int)getGPURank($component2) == 0) {
            $response['result'] = 'false';
        } else {
            if ((int)getGPURank($component1) <= (int)getGPURank($component2)) {
                $response['result'] = 'true';
            } else {
                $response['result'] = 'false';
            }
        }
            break;

        case 'ram':
            if ((float)$component1 >= (float)toByteSize($component2)) {
                $response['result'] = 'true';
            } else {
                $response['result'] = 'false';
            }
            break;

        case 'hdd':
            if ((float)$component1 >= (float)toByteSize($component2)) {
                $response['result'] = 'true';
            } else {
                $response['result'] = 'false';
            }
            break;

        case 'os':
            $pieces1 = explode(" ", preg_replace('/\s+/', ' ', $component1));
            $pieces2 = explode(" ", preg_replace('/\s+/', ' ', $component2));

            $arch1 = trim(implode(" ", array_splice($pieces1, 4, 5)), '()');
            $arch2 = trim(implode(" ", array_splice($pieces2, 3, 4)), '()');

            $response['result'] = $arch1.' : '.$arch2;

            if ($arch2 == $arch1) {
                if (getOSRank($component1) >= getOSRank($component2)) {
                    $response['result'] = 'true';
                } else {
                    $response['result'] = 'false';
                }
                $respnse['result'] = 'first';
            } elseif ($arch1 == '64-bit' && $arch2 == '32-bit') {
                if (getOSRank($component1) >= getOSRank($component2)) {
                    $response['result'] = 'true';
                } else {
                    $response['result'] = 'false';
                }
            } elseif ($arch1 == '32-bit' && $arch2 == '64-bit') {
                $response['result'] = 'false';
            }
            break;

        default:
            $response['result'] = 'false';
            break;
    }

    echo json_encode($response);
});

$app->post('/scan', function() use ($app) {
    $token         = $app->request->post('token');
    $username      = $app->request->post('username');
    $computer_name = $app->request->post('computer_name');

    $content = array();
    $app->response()->header('Content-Type', 'application/json');

    $userExists = R::findOne(DB_TABLE_PREFIX.'users', 'token = :token AND username = :username AND computer_name = :computer_name ', array(
    	':token' => $token,
    	':username' => $username,
    	':computer_name' => $computer_name
    ));

    if (!empty($userExists))
    {
    	$newscan = R::dispense(DB_TABLE_PREFIX.'scans');

        $newscan->user_id = $userExists->id;
        $newscan->bios= $app->request->post('bios');
        $newscan->cpu = $app->request->post('cpu');
        $newscan->gpu = $app->request->post('gpu');
        $newscan->ram = $app->request->post('ram');
        $newscan->hdd = $app->request->post('hdd');
        $newscan->os  = $app->request->post('os');
        $newscan->date_time = date("Y-m-d H:i:s");

        R::store($newscan);

        echo $newscan->id;
    } else {
        $newuser = R::dispense(DB_TABLE_PREFIX.'users');

        $newuser->token = $token;
        $newuser->username = $username;
        $newuser->computer_name = $computer_name;

        R::store($newuser);

        $user = R::findOne('users', 'token = :token AND username = :username AND computer_name = :computer_name ', array(
    	    ':token' => $token,
    	    ':username' => $username,
    	    ':computer_name' => $computer_name
    	));
        if (!empty($user))
        {
            $newscan = R::dispense('scans');

            $newscan->user_id = $user->id;
            $newscan->bios= $app->request->post('bios');
            $newscan->cpu = $app->request->post('cpu');
            $newscan->gpu = $app->request->post('gpu');
            $newscan->ram = $app->request->post('ram');
            $newscan->hdd = $app->request->post('hdd');
            $newscan->os = $app->request->post('os');
            $newscan->date_time = date("Y-m-d H:i:s");

            R::store($newscan);

            echo $newscan->id;
        }
    }
});

$app->get('/software/:id', function ($id) use ($app) {
    $content = array();
    $app->response()->header('Content-Type', 'application/json');

    $game = R::findOne(DB_TABLE_PREFIX.'software', 'id=?', array($id));
    if (!empty($game))
    {
        echo $game;
    } else {
        $content = array("message" => "No such software");
        echo json_encode($content);
    }
});

$app->get('/software/:id/minReq', function ($id) use ($app) {
    $content = array();
    $app->response()->header('Content-Type', 'application/json');

    $game = R::findOne(DB_TABLE_PREFIX.'requirements', 'software_id=? AND type="min"', array($id));
    if (!empty($game))
    {
        echo $game;
    } else {
        $content = array("message" => "No such software");
        echo json_encode($content);
    }
});

$app->get('/software/:id/optReq', function ($id) use ($app) {
    $content = array();
    $app->response()->header('Content-Type', 'application/json');

    $game = R::findOne(DB_TABLE_PREFIX.'requirements', 'software_id=? AND type="opt"', array($id));
    if (!empty($game))
    {
        echo $game;
    } else {
        $content = array("message" => "No such software");
        echo json_encode($content);
    }
});

$app->get('/softwares', function () use ($app) {
    $softwaresList = R::find(DB_TABLE_PREFIX.'software');
    $app->response()->header('Content-Type', 'application/json');

    echo json_encode(R::exportAll($softwaresList));
});

$app->post('/search', function () use ($app) {
    $query = $app->request->post('query');
    $type = $app->request->post('type');

    $app->response()->header('Content-Type', 'application/json');

    if (!empty($query)) {
        $result = R::getAll("SELECT * FROM `".DB_TABLE_PREFIX."software` WHERE `type` = '".$type."' AND `name` LIKE '%".$query."%' LIMIT 3");
        echo json_encode($result);
    } else {
        echo '';
    }
});

$app->get('/shop/search/:type/:query', function ($type, $query) use ($app) {
    $apiurl = "https://shop.benchmark.rs/service/shop/e-kgb/ty3ert5/?keyword=";
    $rank = 1;
    $id = 1;
    $brand = "";
    $found = false;

    $app->response()->header('Content-Type', 'application/json');
    $response = array();
    $response['query'] = $query;
    $response['queries'] = $query.';';

    switch($type) {
        case 'cpu':
            while($found == false && $rank > 0) {
                foreach (getAllUnderID('cpu', getCPUID($query)) as $object) {
                    $query = getInfoByRank('cpu', $rank)['Model'];

                    if($xml = simplexml_load_file($apiurl.$object['Model'], null, LIBXML_NOCDATA)) {
                        if ($xml->message == "Service target found") {
                            if ($xml->list->item->subcategory == "<![CDATA[Procesori]]>") {
                                $found = true;

                                $response['name'] = $xml->list->item->name;
                                $response['image'] = $xml->list->item->image;
                                $response['price'] = $xml->list->item->price;
                                $response['url'] = $xml->list->item->url;

                                $response['message'] = 'Product found.';
                                break;
                            }
                        }
                    }
                }
            }
            break;

        case 'gpu':
            while($found == false && $rank > 0 && $id > 0) {
                foreach (getAllUnderID('gpu', getGPUID($query)) as $object) {
                    $query = getInfoByRank('gpu', $rank)['Model'];

                    if($xml = simplexml_load_file($apiurl.$object['Model'], null, LIBXML_NOCDATA)) {
                        if ($xml->message == "Service target found") {
                            if ($xml->list->item->subcategory == "<![CDATA[Grafičke karte]]>") {
                                $found = true;

                                $response['name'] = $xml->list->item->name;
                                $response['image'] = $xml->list->item->image;
                                $response['price'] = $xml->list->item->price;
                                $response['url'] = $xml->list->item->url;

                                $response['message'] = 'Product found.';
                                break;
                            }
                        }
                    }
                }
            }
            break;

        case 'os':
            if($xml = simplexml_load_file($apiurl.''.$query, null, LIBXML_NOCDATA)) {
                if ($xml->message == "Service target found") {
                    $found = true;

                    $response['name'] = $xml->list->item->name;
                    $response['image'] = $xml->list->item->image;
                    $response['price'] = $xml->list->item->price;
                    $response['url'] = $xml->list->item->url;

                    $response['message'] = 'Product found.';
                }
            }
            break;
    }

    echo json_encode($response);
});

$app->get('/company', function() use ($app) {
    echo 'einutech';
});

$app->run();
?>
