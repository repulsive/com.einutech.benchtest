<?php

define('URL', 'http://localhost/com.einutech.benchtest/');

define('LIBS', 'libs/');
define('AVATAR_PATH', 'public/avatars/');

define('COOKIE_RUNTIME', 1209600);
define('COOKIE_DOMAIN', 'localhost');

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_TABLE_PREFIX' 'bt_')

define("FEEDBACK_UNKNOWN_ERROR", "Unknown error occured!");
