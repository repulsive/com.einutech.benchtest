<?php

class Admin extends Controller {
    function __construct() {
        parent::__construct();
    }

    function requirements($id = 0) {
        $this->view->id = $id;
	    $this->view->render('admin/requirements');
    }

    function software() {
        $this->view->render('admin/software');
    }
}
?>
