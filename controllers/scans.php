<?php

class Scans extends Controller {

    function __construct() {

        parent::__construct();
    }

    function index() {
	    $this->view->render('scans/index');
    }

    function info($id = 0) {
    	if ($this->model->scanExists($id) == true) {
    	    $scan = $this->model->getScanInfo($id);
    	    $this->view->scan = $scan;

            $user = $this->model->getUserProfile($scan->user_id);
            $this->view->user = $user;

            $software = $this->model->getPopularSoftware();
            $this->view->softwares = $software;

    	    $this->view->render('scans/info');
    	} else {
    	    header('location: '.URL.'scans/');
    	}
    }
}
?>
