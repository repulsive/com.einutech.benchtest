<?php

class Users extends Controller {

    function __construct() {

        parent::__construct();
    }

    function index() {
	$this->view->render('users/index');
    }

    function profile($id) {
        if ($this->model->userExists($id) == true) {
            $user = $this->model->getUserProfile($id);
            $scans = $this->model->getUserScans($id);
            $this->view->user = $user;
            $this->view->scans = $scans;
            $this->view->render('users/profile');
        } else {
            header('location: '.URL.'users/');
        }
    }
}
?>
