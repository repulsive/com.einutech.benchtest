<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    echo '<!DOCTYPE html><html><head><title>Einutech MVC Framework</title><style>p{font-family: "Verdana", sans-serif;}p span{color: #D9042B;}</style></head><body><p>Sorry, <span>Einutech MVC Framework</span> does not run on a PHP version smaller than 5.3.7!</p></body></html>';
    exit();
}
require 'libs/external/einutech.Extensions.php';
require 'config/config.php';

function autoload($class) {
    if (file_exists(LIBS . $class . ".php")) {
        require LIBS . $class . ".php";
    } else {
        require LIBS . "external/" . $class . ".php";
    }
}

spl_autoload_register("autoload");

$app = new Bootstrap();
