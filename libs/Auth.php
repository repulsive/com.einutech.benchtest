<?php

class Auth {
    public static function RequireLogin($UserLevel) {
        if ($UserLevel == 2) {
            Session::init();
            if (!isset($_SESSION['user_logged_in'])) {
                Session::destroy();
                header('location: ' . URL);            
            }
        }
        
        if ($UserLevel == 1) {
            if (isset($_SESSION['user_logged_in'])) {
                header('location: ' . URL);
            }
        }
        
        if ($UserLevel == 0) {
            return;
        }
    }
}
