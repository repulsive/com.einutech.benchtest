<?php

class Controller {

    function __construct() {

        Session::init();
        
        if (!isset($_SESSION['user_logged_in']) && isset($_COOKIE['rememberme'])) {

            header('location: ' . URL . 'login/loginwithcookie');
        }        

        $this->view = new View();
    }

    public function loadModel($name) {

        $path = 'models/' . $name . '_model.php';

        if (file_exists($path)) {
            require 'models/' . $name . '_model.php';

            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }
}
