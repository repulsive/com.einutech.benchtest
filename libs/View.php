<?php

class View {

    public function render($filename) {
        require 'views/_templates/head.php';
        require 'views/_templates/header.php';
        require 'views/' . $filename . '.php';
        require 'views/_templates/footer.php';
    }

    private function checkForActiveController($filename, $navigation_controller) {

        $splitted_filename = explode("/", $filename);
        $active_controller = $splitted_filename[0];

        if ($active_controller == $navigation_controller) {

            return true;

        } else {

            return false;
        }

    }

    private function checkForActiveAction($filename, $navigation_action) {

        $splitted_filename = explode("/", $filename);
        $active_action = $splitted_filename[1];

        if ($active_action == $navigation_action) {

            return true;

        } else {

            return false;
        }

    }

    private function checkForActiveControllerAndAction($filename, $navigation_controller_and_action) {

        $splitted_filename = explode("/", $filename);
        $active_controller = $splitted_filename[0];
        $active_action = $splitted_filename[1];

        $splitted_filename = explode("/", $navigation_controller_and_action);
        $navigation_controller = $splitted_filename[0];
        $navigation_action = $splitted_filename[1];

        if ($active_controller == $navigation_controller AND $active_action == $navigation_action) {

            return true;

        } else {

            return false;
        }

    }

}
