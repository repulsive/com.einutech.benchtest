<?php

function formatBytes($size, $decimals = 2){
    $unit = array( '0' => 'B',
                   '1' => 'KB',
                   '2' => 'MB',
                   '3' => 'GB',
                   '4' => 'TB',
                   '5' => 'PB',
                   '6' => 'EB',
                   '7' => 'ZB',
                   '8' => 'YB' );

    for($i = 0; $size >= 1024 && $i <= count($unit); $i++){
        $size = $size / 1024;
    }

    return round($size, $decimals).' '.$unit[$i];
}

function toByteSize($p_sFormatted) {
    $aUnits = array( 'B'  => 0,
                     'KB' => 1,
                     'MB' => 2,
                     'GB' => 3,
                     'TB' => 4,
                     'PB' => 5,
                     'EB' => 6,
                     'ZB' => 7,
                     'YB' => 8 );
                     
    $sUnit = strtoupper(trim(substr($p_sFormatted, -2)));
    if (intval($sUnit) !== 0) {
        $sUnit = 'B';
    }
    if (!in_array($sUnit, array_keys($aUnits))) {
        return false;
    }
    $iUnits = trim(substr($p_sFormatted, 0, strlen($p_sFormatted) - 2));
    if (!intval($iUnits) == $iUnits) {
        return false;
    }
    return $iUnits * pow(1024, $aUnits[$sUnit]);
}

function contains($string, $instring) {
    if (strpos($instring, $string) !== false) {
        return true;
    } else {
        return false;
    }
}

?>
