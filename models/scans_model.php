<?php

class Scans_Model extends Model {
    public $errors = array();

    public function __construct() {
        parent::__construct();
    }

    public function getUserProfile($id) {
        $sth = $this->db->prepare("SELECT * FROM ".DB_TABLE_PREFIX."users WHERE id = :id");
        $sth->execute(array(':id' => $id));

        $user = $sth->fetch();

        return $user;
    }

    public function getScanInfo($id) {
        $sth = $this->db->prepare("SELECT * FROM ".DB_TABLE_PREFIX."scans WHERE id = :id");
        $sth->execute(array(':id' => $id));

        $user = $sth->fetch();

        return $user;
    }

    public function getPopularSoftware() {
        $sth = $this->db->prepare("SELECT * FROM ".DB_TABLE_PREFIX."software LIMIT 10");
        $sth->execute();

        $softwares = array();

        foreach($sth->fetchAll() as $software) {
            $softwares[$software->id] = new stdClass();

            $softwares[$software->id]->id = $software->id;
            $softwares[$software->id]->name = $software->name;
            $softwares[$software->id]->image = $software->image;
        }

        return $softwares;
    }

    public function getUserScans($id) {
        $sth = $this->db->prepare("SELECT * FROM ".DB_TABLE_PREFIX."scans WHERE user_id = :id");
        $sth->execute(array(':id' => $id));

        $scans = array();

        foreach($sth->fetchAll() as $scan) {
            $scans[$scan->id] = new stdClass();

            $scans[$scan->id]->id = $scan->id;
            $scans[$scan->id]->date_time = $scan->date_time;
        }

        return $scans;
    }

    public function scanExists($id) {
        $sth = $this->db->prepare("SELECT * FROM ".DB_TABLE_PREFIX."scans WHERE id = :id");
        $sth->execute(array(':id' => $id));

        $count =  $sth->rowCount();

        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }
}
