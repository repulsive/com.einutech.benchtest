var URL = "http://benchtest.singularminds.eu/";
var isShown = false;

function ShowDropdown() {
	$('#profile').toggleClass('sel');
	$('.menu').slideToggle(250);
	$('#uparrow').slideToggle(0);
}

$('.dropdown').each(function(index, element) {
    $(element).find('font').on('click', function(e) {
        $(element).find('.menu').slideToggle();
    });

    $(element).find('.menu').find('button').each(function(i, el) {
        $(el).on('click', function(e) {
            $(element).find('.menu').slideUp();
            $(element).attr('data-type', $(el).attr('data-type'));
            $(element).find('font').html($(el).html());
        });
    });
});

function contains(string, instring) {
    if (instring.indexOf(string) > -1) {
        return true;
    } else {
        return false;
    }
}

function SelectSoftware(id) {
    $('#search_results').html("");
    $('.software img').each(function() {

    });

    $('.software[data-id="' + id + '"] img');

    $.get(URL + 'api/software/' + id).done(function(data) {
        if(isShown == false) {
            $('#software').slideDown();
            isShown = true;
    		$('#software > .col-md-8 > img').attr('src', data.image);
    		$('#software > .col-md-8 > .softwareinfo > h2').html(data.name)
    		$('#software > .col-md-8 > .softwareinfo > p').html(data.description.substring(0, 400) + '...');
        } else {
            $('#software').fadeOut(300, function() {
                $('#software').fadeIn(300);
                $('#software > .col-md-8 > img').attr('src', data.image);
        		$('#software > .col-md-8 > .softwareinfo > h2').html(data.name)
        		$('#software > .col-md-8 > .softwareinfo > p').html(data.description.substring(0, 400) + '...');
            });
        }
	});

    $.get(URL + 'api/software/' + id + '/minReq').done(function(data) {
        $('#cpu.minReq').html('');
        $('#gpu.minReq').html('');
        $('#ram.minReq').html('');
        $('#hdd.minReq').html('');
        $('#os.minReq').html('');

        $('tr.cpu').each(function(index, element) {
            if (contains('AMD', $(element).find('#cpu.mySpec').attr('data-cpu'))) {
                $(element).find('#cpu.minReq').html(data.cpu_amd);
            } else {
                $(element).find('#cpu.minReq').html(data.cpu_intel);
            }
        });

        $('tr.gpu').each(function(index, element) {
            if (contains('Intel', $(element).find('#gpu.mySpec').attr('data-gpu')) || contains('NVIDIA', $(element).find('#gpu.mySpec').attr('data-gpu'))) {
                $(element).find('#gpu.minReq').html(data.gpu_nvidia);
            } else {
                $(element).find('#gpu.minReq').html(data.gpu_amd);
            }
        });

        $('#ram.minReq').html(data.ram);
        $('#hdd.minReq').html(data.free_hdd);
        $('#os.minReq').html(data.os);

        $('tr.cpu').each(function(index, element) {
            Compare($(element).find('#cpu.mySpec').attr('data-cpu'), $(element).find('#cpu.minReq').html(), 'cpu', element, 'min');
        });
        $('tr.gpu').each(function(index, element) {
            Compare($(element).find('#gpu.mySpec').attr('data-gpu'), $(element).find('#gpu.minReq').html(), 'gpu', element, 'min');
        });
        $('tr.ram').each(function(index, element) {
            Compare($(element).find('#ram.mySpec').attr('data-value'), data.ram, 'ram', element, 'min');
        });
        $('tr.hdd').each(function(index, element) {
            Compare($(element).find('#hdd.mySpec').attr('data-value'), data.free_hdd, 'hdd', element, 'min');
        });
        $('tr.os').each(function(index, element) {
            Compare($(element).find('#os.mySpec').attr('data-os'), data.os, 'os', element, 'min');
        });
    });

    $.get(URL + 'api/software/' + id + '/optReq').done(function(data) {
        $('#cpu.optReq').html('');
        $('#gpu.optReq').html('');
        $('#ram.optReq').html('');
        $('#hdd.optReq').html('');
        $('#os.optReq').html('');

        $('tr.cpu').each(function(index, element) {
            if (contains('AMD', $(element).find('#cpu.mySpec').attr('data-cpu'))) {
                $(element).find('#cpu.optReq').html(data.cpu_amd);
            } else {
                $(element).find('#cpu.optReq').html(data.cpu_intel);
            }
        });

        $('tr.gpu').each(function(index, element) {
            if (contains('Intel', $(element).find('#gpu.mySpec').attr('data-gpu')) || contains('NVIDIA', $(element).find('#gpu.mySpec').attr('data-gpu'))) {
                $(element).find('#gpu.optReq').html(data.gpu_nvidia);
            } else {
                $(element).find('#gpu.optReq').html(data.gpu_amd);
            }
        });

        $('#ram.optReq').html(data.ram);
        $('#hdd.optReq').html(data.free_hdd);
        $('#os.optReq').html(data.os);

        $('tr.cpu').each(function(index, element) {
            Compare($(element).find('#cpu.mySpec').attr('data-cpu'), $(element).find('#cpu.optReq').html(), 'cpu', element, 'opt');
        });
        $('tr.gpu').each(function(index, element) {
            Compare($(element).find('#gpu.mySpec').attr('data-gpu'), $(element).find('#gpu.optReq').html(), 'gpu', element, 'opt');
        });
        $('tr.ram').each(function(index, element) {
            Compare($(element).find('#ram.mySpec').attr('data-value'), data.ram, 'ram', element, 'opt');
        });
        $('tr.hdd').each(function(index, element) {
            Compare($(element).find('#hdd.mySpec').attr('data-value'), data.free_hdd, 'hdd', element, 'opt');
        });
        $('tr.os').each(function(index, element) {
            Compare($(element).find('#os.mySpec').attr('data-os'), data.os, 'os', element, 'opt');
        });
    });
}

function Compare(Component1, Component2, Type, Element, MinOpt) {
    if (MinOpt == 'min') {
        var ElementMinOpt = $(Element).find('.minReq');
    } else if (MinOpt == 'opt') {
        var ElementMinOpt = $(Element).find('.optReq');
    }

    var request = $.post(URL + 'api/compare',
        { component1: Component1,
          component2: Component2,
          type: Type });

    request.done(function(data) {
        console.log(Type + ' [' + Component1 + ' = ' + Component2 + ' : ' + data.result + ']');
        switch(data.result) {
            case 'true':
                $(ElementMinOpt).css('background-color', '#afe9b2');
                break;

            case 'false':
                $(ElementMinOpt).css('background-color', '#eda7a7');
                var $url = URL + 'api/shop/search/' + Type + '/' + Component2;
                $.ajax({
                    type: "GET",
                    url: $url,
                    cache: false,
                    dataType: "json",
                    success: function(data) {
                        $(ElementMinOpt).html('<img class="product-image" src="' + data.image[0].replace("<![CDATA[", "").replace("]]>", "") + '" /><div class="product-info"><p class="product-name">' + data.name[0].replace("<![CDATA[", "").replace("]]>", "") + '</p><p class="product-price">' + data.price[0] + ' RSD</p></div>');
                        $(ElementMinOpt).wrapInner('<a href="' + data.url[0].replace("<![CDATA[", "").replace("]]>", "") + '" target="_blank"></a>');
                    }
                });
                break;

            default:
                $(ElementMinOpt).css('background-color', '#eda7a7');
                break;
        }
    });
}

$("#search").keyup(function() {
    var query = $("#search").val();
    var type = $(".search .dropdown").attr('data-type');

    if (query != '') {
        $.post(URL + 'api/search', { query: query, type: type }).done(function(data) {
            var $html = '';

            $.each(data, function() {
                var desc = this.description;
                desc = desc.substr(0, 100);
                desc = desc.substr(0, Math.min(desc.length, desc.lastIndexOf(" ")));

                $html += '<div class="item" onclick="SelectSoftware(' + this.id + ')"><img src="' + this.image + '" /><div class="info"><p class="name">' + this.name + '</p><p class="desc">' + desc + '...</p></div></div>';
            });

            $('#search_results').html($html);
        });
    } else {
        $('#search_results').html("");
    }
});
