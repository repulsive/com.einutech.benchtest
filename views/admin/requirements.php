<div class="page container">
    <form action="<?php echo URL; ?>api/admin/insertRequirements" method="POST">
        <label>Type:</label><br>
        <input type="radio" name="Type" value="min" checked> Minimum<br>
        <input type="radio" name="Type" value="opt"> Optimal<br><br><br>
        <input type="number" placeholder="Software ID" name="SoftwareID" value="<?php echo $this->id; ?>" required /><br><br>
        <input type="text" placeholder="Intel CPU" name="CPU_Intel" required /><br><br>
        <input type="text" placeholder="AMD CPU" name="CPU_AMD" required /><br><br>
        <input type="text" placeholder="nVidia GPU" name="GPU_Nvidia" required /><br><br>
        <input type="text" placeholder="AMD GPU" name="GPU_AMD" required /><br><br>
        <input type="text" placeholder="RAM" name="RAM" required /><br><br>
        <input type="text" placeholder="Free HDD Space" name="HDD" required /><br><br>
        <input type="text" placeholder="Operating System" name="OS" required /><br><br>
        <input type="submit" value="Submit Data" />  <a href="<?php echo URL; ?>admin/software">Add new Software</a>
    </form>
</div>
