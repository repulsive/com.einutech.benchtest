<?php
$biosInfo = explode(';', $this->scan->bios);
$cpuInfo = explode(';', $this->scan->cpu);
$gpuInfo = explode(';', $this->scan->gpu);
$ramInfo = explode(';', $this->scan->ram);
$hddInfo = explode(';', $this->scan->hdd);
$osInfo = explode(';', $this->scan->os);

$i = -1;
?>

<div class="page container">
    <div class="search">
        <input id="search" autocomplete="off" type="text" placeholder="Start searching for a game, application or operating system...">
        <div id="search_results"></div>

        <div data-type="game" class="dropdown">
            <font>Games</font>

            <div class="menu">
                <button data-type="game">Games</button>
                <button data-type="app">Apps</button>
                <button data-type="os">OS</button>
            </div>
        </div>
    </div>

    <div class="mostpopularsoftware">
        <h2>Newest Most Popular Software</h2>
        <hr />
        <?php foreach($this->softwares as $software) {
            echo '<a data-id="'.$software->id.'" onclick="SelectSoftware(\''.$software->id.'\');" class="software">';
            echo '<img src="'.$software->image.'" />';
            echo '<p>'.$software->name.'</p>';
            echo '</a>';
        } ?>
    </div>

    <div class="row" id="software">
        <div class="col-md-8">
            <img />
            <div class="softwareinfo">
                <h2></h2>
                <p></p>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
    <a href="<?php echo URL; ?>users/profile/<?php echo $this->scan->user_id; ?>">
        <div class="user">
            <h2><?php echo $this->user->username; ?></h2>
            <p><?php echo $this->user->computer_name; ?></p>
        </div>
    </a>

    <table border="0">
        <thead>
            <tr>
                <th>
                    <p class="title">YOUR PC INFORMATION</p>
                    <p class="subtitle">These are your PC information. If you want to rescan, you can do it here.</p>
                </th>
                <th>
                    <p class="title">MINIMUM REQUIREMENTS</p>
                    <p class="subtitle">These are minimum system requirements for this game.</p>
                </th>
                <th>
                    <p class="title">RECOMMENDED REQUIREMENTS</p>
                    <p class="subtitle">This is the hardware that we recommend for the best enjoyment in the game.</p>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="category">
                <td><img width="16" height="16" src="<?php echo URL; ?>public/img/icons/cpu.png" />&nbsp;&nbsp;CPU</td>
                <td id="req-cpu"></td>
                <td></td>
            </tr>
            <?php foreach($cpuInfo as $cpu) {
                $i++;
                echo '<tr class="cpu data" data-id="'.$i.'">';
                echo '<td id="cpu" class="mySpec" data-cpu="'.$cpu.'">'.$cpu.'</td>';
                echo '<td id="cpu" class="minReq"></td>';
                echo '<td id="cpu" class="optReq"></td>';
                echo '</tr>';
            };
            $i = -1; ?>
            <tr class="separator"><td></td></tr>

            <tr class="category">
                <td><img width="17" height="16" src="<?php echo URL; ?>public/img/icons/gpu.png" />&nbsp;&nbsp;Video Card</td>
                <td id="req-gpu"></td>
                <td></td>
            </tr>
            <?php foreach($gpuInfo as $gpu) {
                $i++;
                echo '<tr class="gpu data" data-id="'.$i.'">';
                echo '<td id="gpu" class="mySpec" data-gpu="'.$gpu.'">'.$gpu.'</td>';
                echo '<td id="gpu" class="minReq"></td>';
                echo '<td id="gpu" class="optReq"></td>';
                echo '</tr>';
            };
            $i = -1; ?>
            <tr class="separator"><td></td></tr>

            <tr class="category">
                <td><img width="24" height="13" src="<?php echo URL; ?>public/img/icons/ram.png" />&nbsp;&nbsp;RAM</td>
                <td id="req-ram"></td>
                <td></td>
            </tr>
            <?php
            $i++;
            echo '<tr class="ram data" data-id="'.$i.'">';
            $ramCap = 0;
            foreach($ramInfo as $ram) {
                $ramData = explode('|', $ram);
                $ramCap += intval($ramData[1]);
            };
            echo '<td id="ram" class="mySpec" data-value="'.$ramCap.'">'.formatBytes($ramCap).'</td>';
            echo '<td id="ram" class="minReq"></td>';
            echo '<td id="ram" class="optReq"></td>';
            echo '</tr>';
            $i = -1; ?>
            <tr class="separator"><td></td></tr>

            <tr class="category">
                <td><img width="15" height="16" src="<?php echo URL; ?>public/img/icons/hdd.png" />&nbsp;&nbsp;Free Disk Space</td>
                <td id="req-hdd"></td>
                <td></td>
            </tr>
            <?php foreach($hddInfo as $hdd) {
                $i++;
                echo '<tr class="hdd data" data-id="'.$i.'">';
                $hddData = explode('|', $hdd);
                echo '<td id="hdd" class="mySpec" data-value="'.$hddData[2].'">'.$hddData[1].' ('.$hddData[0].'):&nbsp&nbsp&nbsp<strong>'.formatBytes($hddData[2]).'</strong></td>';
                echo '<td id="hdd" class="minReq"></td>';
                echo '<td id="hdd" class="optReq"></td>';
                echo '</tr>';
            };
            $i = -1; ?>
            <tr class="separator"><td></td></tr>

            <tr class="category">
                <td><img width="15" height="16" src="<?php echo URL; ?>public/img/icons/os.png" />&nbsp;&nbsp;OS</td>
                <td id="req-os"></td>
                <td></td>
            </tr>
            <?php foreach($osInfo as $os) {
                $i++;
                echo '<tr class="os data" data-id="'.$i.'">';
                $osData = explode('|', $os);
                echo '<td id="os" class="mySpec" data-os="'.$osData[0].' ('.$osData[1].')">'.$osData[0].' ('.$osData[1].')</td>';
                echo '<td id="os" class="minReq"></td>';
                echo '<td id="os" class="optReq"></td>';
                echo '</tr>';
            };
            $i = -1; ?>
            <tr class="separator"><td></td></tr>
        </tbody>
    </table>
</div>
