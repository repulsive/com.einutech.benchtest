<div class="page container">
	<div class="user" style="overflow: hidden;">
		<div style="overflow: hidden; display: inline-block; float: left; width: 100px;">
			<h2 style="margin-top: 0; margin-bottom: 0;"><?php echo $this->user->username; ?></h2>
			<p><?php echo $this->user->computer_name; ?></p>
		</div>
		<div style="overflow: hidden; display: inline-block; float: left;">
			<kbd style="margin-top: 15px; display: inline-block;"><?php echo $this->user->token; ?></kbd>
		</div>
	</div>

	<table>
		<thead>
			<tr>
				<th colspan="3">ALL SCANS</th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Time</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($this->scans as $scan) {
					echo '<tr>';
					echo '<td>'.$scan->id.'</td>';
					echo '<td>'.date_format(date_create($scan->date_time),"d.m.Y").'</td>';
					echo '<td>'.date_format(date_create($scan->date_time),"H:i:s").'</td>';
					echo '</tr>';
				}
			?>
		</tbody>
	</table>
</div>
